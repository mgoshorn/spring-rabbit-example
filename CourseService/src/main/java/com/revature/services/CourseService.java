package com.revature.services;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.revature.messaging.producer.CourseMessenger;
import com.revature.models.Course;
import com.revature.repositories.CourseRepository;

@Service
public class CourseService {

	private CourseRepository courseRepository; 
	private CourseMessenger courseMessenger;
	
	public Course getCourse(int id) {
		return courseRepository.findById(id)
				.orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
	}
	
	@Transactional
	public Course saveCourse(Course course) {
		Course savedCourse = courseRepository.save(course);
		courseMessenger.sendCreationMessage(savedCourse);
		return savedCourse;
	}
	
	public Course updateCourse(Course course) {
		Course updatedCourse = courseRepository.save(course);
		return updatedCourse;
	}

	@Inject
	public CourseService(CourseRepository courseRepository, CourseMessenger courseMessenger) {
		super();
		this.courseRepository = courseRepository;
		this.courseMessenger = courseMessenger;
	}
	
	

}
