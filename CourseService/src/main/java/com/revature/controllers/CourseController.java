package com.revature.controllers;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.models.Course;
import com.revature.services.CourseService;

@RequestMapping("/course")
@RestController
public class CourseController {

	private CourseService courseService;
	
	@GetMapping("/{id}")
	public Course getCourse(@PathVariable int id) {
		return courseService.getCourse(id);
	}
	
	@PostMapping("/")
	public Course saveCourse(@RequestBody Course course) {
		return courseService.saveCourse(course);
	}
	
	@PutMapping("/")
	public Course updateCourse(@RequestBody Course course) {
		return courseService.updateCourse(course);
	}
	@Inject
	public CourseController(CourseService courseService) {
		this.courseService = courseService;
	}
}
