package com.revature.messaging.config;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
public class RabbitConfig {

	public final String EXCHANGE;
	public final String COURSE_CREATE_ROUTE;
	public final String COURSE_DELETE_ROUTE;
	public final String COURSE_UPDATE_ROUTE;

	public RabbitConfig(@Value("${spring.jpa.rabbitmq.exchange:assignforce}") String exchange,
			@Value("${spring.jpa.rabbitmq.routes.course.create:assignforce.course.create}") String courseCreateRoute,
			@Value("${spring.jpa.rabbitmq.routes.course.delete:assignforce.course.delete}") String courseDeleteRoute,
			@Value("${spring.jpa.rabbitmq.routes.course.update:assignforce.course.update}") String courseUpdateRoute) {
		this.EXCHANGE = exchange;
		this.COURSE_CREATE_ROUTE = courseCreateRoute;
		this.COURSE_DELETE_ROUTE = courseDeleteRoute;
		this.COURSE_UPDATE_ROUTE = courseUpdateRoute;
	}

	public String getEXCHANGE() {
		return EXCHANGE;
	}

	public String getCOURSE_CREATE_ROUTE() {
		return COURSE_CREATE_ROUTE;
	}

	public String getCOURSE_DELETE_ROUTE() {
		return COURSE_DELETE_ROUTE;
	}

	public String getCOURSE_UPDATE_ROUTE() {
		return COURSE_UPDATE_ROUTE;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(jacksonProducerConverter());
		return rabbitTemplate;
	}

	@Bean
	public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
		DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
		factory.setMessageConverter(jacksonConsumerConverter());
		return factory;
	}

	@Bean
	public Jackson2JsonMessageConverter jacksonProducerConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public MappingJackson2MessageConverter jacksonConsumerConverter() {
		return new MappingJackson2MessageConverter();
	}
}
