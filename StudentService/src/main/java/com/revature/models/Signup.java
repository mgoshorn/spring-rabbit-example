package com.revature.models;

public class Signup {
	private int id;
	private int studentId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + studentId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signup other = (Signup) obj;
		if (id != other.id)
			return false;
		if (studentId != other.studentId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Signup [id=" + id + ", studentId=" + studentId + "]";
	}
	public Signup(int id, int studentId) {
		super();
		this.id = id;
		this.studentId = studentId;
	}
	public Signup() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
