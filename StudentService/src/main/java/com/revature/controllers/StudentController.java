package com.revature.controllers;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.models.Student;
import com.revature.services.StudentService;

@RestController()
@RequestMapping("/students")
public class StudentController {

	StudentService studentService;
	
	@GetMapping("/{id}")
	public Student getStudent(@PathVariable int id) {
		return studentService.getStudent(id);
	}
	
	@PostMapping("/")
	public Student saveStudent(@RequestBody Student student) {
		return studentService.saveStudent(student);
	}
	
	@PutMapping("/")
	public Student updateStudent(@RequestBody Student student) {
		return studentService.updateStudent(student);
	}

	@Inject
	public StudentController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}
	
	
}
