package com.revature.services;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.revature.models.Student;
import com.revature.repositories.StudentRepository;

@Service
public class StudentService {
	private StudentRepository studentRepository;

	@Inject
	public StudentService(StudentRepository studentRepository) {
		super();
		this.studentRepository = studentRepository;
	}

	public Student getStudent(int id) {
		return studentRepository.findById(id).orElseThrow(
				() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
	}

	public Student saveStudent(Student student) {
		Student newStudent = studentRepository.save(student);
		return newStudent;
	}

	public Student updateStudent(Student student) {
		Student updatedStudent = studentRepository.save(student);
		return updatedStudent;
	}
}
