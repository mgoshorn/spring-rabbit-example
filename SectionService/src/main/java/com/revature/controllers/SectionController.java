package com.revature.controllers;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.models.Section;
import com.revature.services.SectionService;

@RestController()
@RequestMapping("/sections")
public class SectionController {

	private SectionService sectionService;

	@GetMapping("/{id}")
	public Section getSection(@PathVariable int id) {
		return sectionService.getSection(id);
	}

	@PostMapping("/")
	public Section saveSection(@RequestBody Section section) {
		return sectionService.saveSection(section);
	}

	@PutMapping("/{id}/capacity/{capacity}")
	public Section setSectionCapacity(@PathVariable int id, @PathVariable int capacity) {
		return sectionService.setCapacity(id, capacity);
	}
	
	@PutMapping("/")
	public Section updateSection(@RequestBody Section section) {
		return sectionService.updateSection(section);
	}

	@Inject
	public SectionController(SectionService sectionService) {
		super();
		this.sectionService = sectionService;
	}

}
