package com.revature.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="section_signups")
public class Signup implements Comparable<Signup> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "signup_time")
	private LocalDateTime signupTime;

	@Column(name = "student_id")
	private int studentId;

	public int compareTo(Signup other) {
		return signupTime.compareTo(other.signupTime);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getSignupTime() {
		return signupTime;
	}

	public void setSignupTime(LocalDateTime signupTime) {
		this.signupTime = signupTime;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((signupTime == null) ? 0 : signupTime.hashCode());
		result = prime * result + studentId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signup other = (Signup) obj;
		if (id != other.id)
			return false;
		if (signupTime == null) {
			if (other.signupTime != null)
				return false;
		} else if (!signupTime.equals(other.signupTime))
			return false;
		if (studentId != other.studentId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Signup [id=" + id + ", signupTime=" + signupTime + ", studentId=" + studentId + "]";
	}

	public Signup(int id, LocalDateTime signupTime, int studentId) {
		super();
		this.id = id;
		this.signupTime = signupTime;
		this.studentId = studentId;
	}

	public Signup() {
		super();
	}

}
