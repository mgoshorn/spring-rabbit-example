package com.revature.services;

import java.util.Collections;

import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import com.revature.messaging.messengers.SignupMessenger;
import com.revature.models.Section;
import com.revature.models.Signup;
import com.revature.repositories.SectionRepository;

@Service
public class SectionService {

	SectionRepository sectionRepository;
	SignupMessenger signupMessenger;
	Logger log = Logger.getLogger(SectionService.class);
	
	public Section getSection(int id) {
		return sectionRepository.findById(id)
				.orElseThrow( () -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
	}

	public Section saveSection(Section section) {
		Section newSection = sectionRepository.save(section);
		return newSection;
	}

	@Transactional
	public Section updateSection(Section section) {
		Section updatedSection = sectionRepository.save(section);
		bumpStudents(updatedSection);
		return sectionRepository.save(section);
	}

	@Inject
	public SectionService(SectionRepository sectionRepository,
				SignupMessenger signupMessenger) {
		super();
		this.sectionRepository = sectionRepository;
		this.signupMessenger = signupMessenger;
	}

	@Transactional(propagation=Propagation.MANDATORY)
	private void bumpStudents(Section section) {
		// order the signup list
		Collections.sort(section.getSignups());
		
		// while the room size is greater than the signups
		// bump the last person in the signup list
		while(section.getCapacity() < section.getSignups().size()) {
			
			Signup signup = section.getSignups().remove(section.getSignups().size()-1);
			log.info("Bumping student: " + signup);
			// Send message to e-mail student about removal from course
			signupMessenger.sendDeletionMessage(signup);
		}
	}

	public Section setCapacity(int id, int capacity) {
		Section section = getSection(id);
		section.setCapacity(capacity);
		return updateSection(section);
	}
	
	
}
